# Generating the data for 3P-Circuit ORAM for the PRAC paper

## Building the docker

  - `cd ../docker`
  - Build the docker image with `./build-docker`
  - Start the dockers with `./start-docker` (This will start three dockers, each running one of the parties.)

## Setting the network parameters

  - `./set-networking 30ms 100mbit`

## Generating raw data for the binary search experiments

  - `cd ../prac`
  - Run the reproduction script `./repro` with one of the following arguments:
    - <code>./repro test</code>: Run a short (about 30 seconds) "kick-the-tires" test.
      You should see output like the following:

            Running test experiment...
            Wed Nov  1 03:59:19 PM EDT 2023: Running 16 1 ...

            # Test output

            CircuitORAMOnln read 16 1 6.289 s
            CircuitORAMOnln read 16 1 355.3125 KiB
            CircuitORAMTotl read 16 1 7.145 s
            CircuitORAMTotl read 16 1 2478.5 KiB  

            # End test output

      The last four lines are the output data points, telling you that a
      Circuit ORAM read test on an ORAM of size 2<sup>16</sup>, with a network
      configuration of 1us latency and 100gbit bandwidth, performing 1
      read operation, took 6.289 s of online time and 7.145 s of total
      (precomputation plus online) time, and 355.3125 KiB of
      online bandwidth and 2478.5 KiB of total bandwidth.  If you've run
      the test before, you will see means and stddevs of all of the output
      data points.  When you run it, the time of course will depend on
      the particulars of your hardware, but the bandwidths used should be
      exactly the values quoted above.

    - <code>./repro all _numiters_</code>: Run all of the tests, doing <code>_numiters_</code> iterations of each.

## When you're done

Stop the docker:

  - `../docker/stop-docker`
