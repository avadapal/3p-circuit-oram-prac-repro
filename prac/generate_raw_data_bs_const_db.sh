#!/bin/bash

# Usage: ./generate_raw_data_bs_const_db.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    now=`date`; echo "$now: Running 4 Binary Searches ..."
    ./docker/run-experiment 20 80 >> "prac/data/log_20_4_bs.out"
    now=`date`; echo "$now: Running 8 Binary Searches ..."
    ./docker/run-experiment 20 160 >> "prac/data/log_20_8_bs.out"
    now=`date`; echo "$now: Running 16 Binary Searches ..."
    ./docker/run-experiment 20 320 >> "prac/data/log_20_16_bs.out"
    now=`date`; echo "$now: Running 32 Binary Searches ..."
    ./docker/run-experiment 20 640 >> "prac/data/log_20_32_bs.out"
    now=`date`; echo "$now: Running 64 Binary Searches ..."
    ./docker/run-experiment 20 1280 >> "prac/data/log_20_64_bs.out"
done
