#!/bin/bash

# Usage: ./generate_raw_data_reads.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    now=`date`; echo "$now: Running 10 reads on DB of size 2^16 ..."
    ./docker/run-experiment 16 10 >> "prac/data/log_16_10_reads.out"
    now=`date`; echo "$now: Running 10 reads on DB of size 2^18 ..."
    ./docker/run-experiment 18 10 >> "prac/data/log_18_10_reads.out"
    now=`date`; echo "$now: Running 10 reads on DB of size 2^20 ..."
    ./docker/run-experiment 20 10 >> "prac/data/log_20_10_reads.out"
    now=`date`; echo "$now: Running 10 reads on DB of size 2^22 ..."
    ./docker/run-experiment 22 10 >> "prac/data/log_22_10_reads.out"
    now=`date`; echo "$now: Running 10 reads on DB of size 2^24 ..."
    ./docker/run-experiment 24 10 >> "prac/data/log_24_10_reads.out"
    now=`date`; echo "$now: Running 10 reads on DB of size 2^26 ..."
    ./docker/run-experiment 26 10 >> "prac/data/log_26_10_reads.out"
    now=`date`; echo "$now: Running 10 reads on DB of size 2^28 ..."
    ./docker/run-experiment 28 10 >> "prac/data/log_28_10_reads.out"
    now=`date`; echo "$now: Running 10 reads on DB of size 2^30 ..."
    ./docker/run-experiment 30 10 >> "prac/data/log_30_10_reads.out"
done
