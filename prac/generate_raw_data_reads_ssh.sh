#!/bin/bash

# Usage: ./generate_raw_data_reads_ssh.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    for n in 1 2 3 4 48 96 144; do
        now=`date`; echo "$now: Running $n reads on size 2^9 ..."
        ./docker/run-experiment-ssh 9 $n >> "prac/data/log_9_${n}_reads.out"
    done
done
