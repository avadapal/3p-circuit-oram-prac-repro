#!/bin/bash

# Usage: ./generate_raw_data_reads_const_db.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    now=`date`; echo "$now: Running 16 reads ..."
    ./docker/run-experiment 20 16 >> "prac/data/log_20_16_reads.out"
    now=`date`; echo "$now: Running 32 reads ..."
    ./docker/run-experiment 20 32 >> "prac/data/log_20_32_reads.out"
    now=`date`; echo "$now: Running 64 reads ..."
    ./docker/run-experiment 20 64 >> "prac/data/log_20_64_reads.out"
    now=`date`; echo "$now: Running 128 reads ..."
    ./docker/run-experiment 20 128 >> "prac/data/log_20_128_reads.out"
    now=`date`; echo "$now: Running 256 reads ..."
    ./docker/run-experiment 20 256 >> "prac/data/log_20_256_reads.out"
    now=`date`; echo "$now: Running 512 reads ..."
    ./docker/run-experiment 20 512 >> "prac/data/log_20_512_reads.out"
    now=`date`; echo "$now: Running 1024 reads ..."
    ./docker/run-experiment 20 1024 >> "prac/data/log_20_1024_reads.out"
    now=`date`; echo "$now: Running 2048 reads ..."
    ./docker/run-experiment 20 2048 >> "prac/data/log_20_2048_reads.out"
done
