#!/bin/bash

# Usage: ./generate_raw_data_bs.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    now=`date`; echo "$now: Running 1 search on DB of size 2^16 ..."
    ./docker/run-experiment 16 16 >> "prac/data/log_16_1_bs.out"
    now=`date`; echo "$now: Running 1 search on DB of size 2^18 ..."
    ./docker/run-experiment 18 18 >> "prac/data/log_18_1_bs.out"
    now=`date`; echo "$now: Running 1 search on DB of size 2^20 ..."
    ./docker/run-experiment 20 20 >> "prac/data/log_20_1_bs.out"
    now=`date`; echo "$now: Running 1 search on DB of size 2^22 ..."
    ./docker/run-experiment 22 22 >> "prac/data/log_22_1_bs.out"
    now=`date`; echo "$now: Running 1 search on DB of size 2^24 ..."
    ./docker/run-experiment 24 24 >> "prac/data/log_24_1_bs.out"
    now=`date`; echo "$now: Running 1 search on DB of size 2^26 ..."
    ./docker/run-experiment 26 26 >> "prac/data/log_26_1_bs.out"
    now=`date`; echo "$now: Running 1 search on DB of size 2^28 ..."
    ./docker/run-experiment 28 28 >> "prac/data/log_28_1_bs.out"
    now=`date`; echo "$now: Running 1 search on DB of size 2^30 ..."
    ./docker/run-experiment 30 30 >> "prac/data/log_30_1_bs.out"
done
