#!/bin/bash

# Usage: ./generate_raw_data_heap_const_db.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    for n in 4 8 16 32; do
        now=`date`; echo "$now: Running $n Heap Extracts ..."
        ./docker/run-experiment 20 $((n*19*6)) >> "prac/data/log_20_${n}_heap.out"
    done
done 
