#!/bin/bash

# Usage: ./generate_raw_data_heap.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    for size in 16 18 20 22 24 26 28 30; do
        now=`date`; echo "$now: Running heap extract on heapsize of 2^${size} ..."
        ./docker/run-experiment $size $((6*(size-1))) >> "prac/data/log_${size}_1_heap.out"
    done
done
